import { useState } from "react";

import "./App.css";

const IconClosed = (props: { onToggle: React.MouseEventHandler }) => {
  const { onToggle } = props;
  return (
    <button onClick={onToggle} className="menuButton">
      <div className="icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
          <path d="M11.414 10l6.293-6.293c.39-.39.39-1.023 0-1.414s-1.023-.39-1.414 0L10 8.586 3.707 2.293c-.39-.39-1.023-.39-1.414 0s-.39 1.023 0 1.414L8.586 10l-6.293 6.293c-.39.39-.39 1.023 0 1.414.195.195.45.293.707.293s.512-.098.707-.293L10 11.414l6.293 6.293c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414L11.414 10z"></path>
        </svg>
      </div>
    </button>
  );
};

const IconOpen = (props: { onToggle: React.MouseEventHandler }) => {
  const { onToggle } = props;
  return (
    <button onClick={onToggle} className="menuButton">
      <div className="icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
          <path d="M19 11H1c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1s-.448 1-1 1zm0-7H1c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1s-.448 1-1 1zm0 14H1c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1s-.448 1-1 1z"></path>
        </svg>
      </div>
    </button>
  );
};

const Menu = (props: {
  isOpen: boolean;
  onToggle: React.MouseEventHandler;
}) => {
  const { isOpen, onToggle } = props;

  const menuClass = isOpen ? "menu open" : "menu";

  return (
    <menu className={menuClass}>
      <div className="navigationHead">
        <span>Menu</span>
        <IconClosed onToggle={onToggle} />
      </div>
      <div className="navigation">
        <div className="main">
          <ul>
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#">Shop</a>
            </li>
            <li>
              <a href="#">About us</a>
            </li>
          </ul>
        </div>

        <div className="secondary">
          <ul>
            <li>
              <a href="#">Contact</a>
            </li>
            <li>
              <a href="#">Track your order</a>
            </li>
            <li>
              <a href="#">Return policy</a>
            </li>
            <li>
              <a href="#">Shipping policy</a>
            </li>
          </ul>
        </div>
      </div>
    </menu>
  );
};

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="App">
      <div className="header">
        <h1>Header</h1>
        <IconOpen onToggle={toggle} />
      </div>
      <Menu isOpen={isOpen} onToggle={toggle} />
    </div>
  );
}

export default App;
